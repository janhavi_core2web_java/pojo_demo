package com.pojo;
import java.util.Scanner;

import javafx.application.Application;
import main.java.com.pojo.PlayerData;


public class Main {
    public static void main(String[] args) {
        System.out.println("Player Info");

        PlayerData pd = new PlayerData();
        
        pd.setPlayerName("Shubham Dubey");
        pd.setCountry("India");
        pd.setAge(29);

        System.out.println("Player Name : "+ pd.getPlayerName());
        System.out.println("Country : "+ pd.getCountry());
        System.out.println("Age : "+ pd.getAge());

        Scanner sc = new Scanner(System.in);

        System.out.println("Enter Player's Name : ");
        String playerName = sc.nextLine();

        System.out.println("Enter Country Name : ");
        String countryName = sc.nextLine();

        System.out.println("Enter Player's Age : ");
        int age = sc.nextInt();

        pd = new PlayerData();

        pd.setPlayerName(playerName);
        pd.setCountry(countryName);
        pd.setAge(age);

        System.out.println("Player Name : "+ pd.getPlayerName());
        System.out.println("Country : "+ pd.getCountry());
        System.out.println("Age : "+ pd.getAge());

        Application.launch(PlayerData.class,args);

    }
}